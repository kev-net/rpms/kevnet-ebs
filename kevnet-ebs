#!/bin/sh

# Constants used within
libexec_dir="@LIBEXEC@"
resize_pct=95

# log outputs logging information
log() {
    local level=$1
    shift
    local msg=$*

    local fatal=false

    # Set level for bare messages to "info"
    if [ "${msg}" == "" ]; then
	msg=${level}
	level=info
    fi

    # Handle level disposition
    if [ "${level@U}" == "DEBUG" ] && [ -z "${EBS_DEBUG}" ]; then
	return 0
    elif [ "${level@U}" == "FATAL" ]; then
	level=error
	fatal=true
    fi

    echo "[${level@U}] ${msg}" >&2

    if [ "${fatal}" == true ]; then
	exit 1
    fi
}

# awsmeta retrieves a given piece of data from the AWS metadata
# server.  The data is specified by the path that follows the
# "meta-data" element of the URL.
awsmeta() {
    local datum=$1
    local -n var=$2

    # If the variable is set, use its value
    if [ -n "${var}" ]; then
	echo "${var}"
	return 0
    fi

    curl -m 5 -s "http://169.254.169.254/latest/meta-data/${datum}"
}

log debug "kevnet-ebs invoked with: $*"

# First, grab the parameters
action=$1
sys_dev=$2

# Does the action make sense?
if [ "${action}" != "start" ] && [ "${action}" != "reload" ] && [ "${action}" != "stop" ]; then
    log fatal "Unknown action \"${action}\""
fi

# Next, convert the /sys device name to the /dev name
dev=$(udevadm info --query=property --property=DEVNAME --value "${sys_dev}" 2>/dev/null)
# shellcheck disable=SC2181
if [ "$?" != 0 ]; then
    log fatal "No device exists for system path \"${sys_dev}\""
fi

log debug "Action ${action} on ${dev} (${sys_dev})"

# Extract all information about the device.  Note that only recent
# versions of lsblk support -y, thus the '||' in the following
# command.
# shellcheck disable=SC2046
eval $(
    (lsblk -bPdOy "${dev}" 2>/dev/null || lsblk -bPdO "${dev}" 2>/dev/null) |
	sed 's/\([A-Z_][A-Z_]*="[^"]*"\) */DEV_\1\n/g'
)

# Is this a root device?
if [ -n "${DEV_PKNAME}" ]; then
    log debug "Device ${dev} is a partition device; skipping..."
    exit 0
fi

# Now we look up the region and instance ID
region=$(awsmeta placement/region AWS_REGION)
instance=$(awsmeta instance-id AWS_INSTANCE)

log debug "AWS region: ${region}; instance ID: ${instance}"

# Don't go on if we don't have the data we need
if [ -z "${region}" ] || [ -z "${instance}" ]; then
    log fatal "Missing AWS region or instance ID"
fi

# Now look up the AWS info
# shellcheck disable=SC2046
eval $(aws --region="${region}" ec2 describe-volumes --filters \
    "Name=attachment.instance-id,Values=${instance}" \
    "Name=attachment.device,Values=${dev}" | jq -r '
        .Volumes[0].Tags[] |
	"AWSTAG_\(.Key | ascii_upcase | gsub("[^A-Z_]+"; "_"))=\(.Value | @sh)"
    ' 2>/dev/null)

# If there's no FSTYPE in the tags, skip it
if [ -z "${AWSTAG_FSTYPE}" ]; then
    log info "Device ${dev} has no associated filesystem tag; skipping..."
    exit 0
fi

# If the FSTYPE isn't recognized, skip it
 if ! [ -r "${libexec_dir}/plugin.${AWSTAG_FSTYPE}" ]; then
    log fatal "Device ${dev} has unrecognized filesystem tag \"${AWSTAG_FSTYPE}\""
fi

# Does the mountpoint make sense?
if [ -n "${AWSTAG_MOUNTPOINT}" ] && [ -f "${AWSTAG_MOUNTPOINT}" ]; then
    log fatal "Device ${dev} mountpoint ${AWSTAG_MOUNTPOINT} is a file"
fi

# Select the fstype to use
fstype=${AWSTAG_FSTYPE}
init_fs=true
if [ -n "${DEV_FSTYPE}" ]; then
    init_fs=false
    if ! [ -r "${libexec_dir}/plugin.${DEV_FSTYPE}" ]; then
	log fatal "Device ${dev} has unknown filesystem type ${DEV_FSTYPE}"
    fi
    fstype=${DEV_FSTYPE}
fi

log debug "Device ${dev} selected filesystem: ${fstype}; loading plugin..."

# shellcheck source=libexec/plugin.ext4
. "${libexec_dir}/plugin.${fstype}"

# Do we need to initialize or resize the filesystem?
if [ "${action}" == "start" ] && [ "${init_fs}" = true ]; then
    log info "Initializing a ${fstype} filesystem on ${dev}"
    init "${dev}" || \
	log fatal "Failed to initialize filesystem on ${dev}"
elif [ "${action}" != "stop" ] && [ -n "${DEV_FSSIZE}" ] && \
	 [ -n "${DEV_SIZE}" ] && \
	 [ $(( "${DEV_FSSIZE}" * 100 / "${DEV_SIZE}" )) -lt "${resize_pct}" ]; then
    if [ -n "${DEV_MOUNTPOINT}" ]; then
	log info "Online-resizing a ${fstype} filesystem on ${dev} from ${DEV_FSSIZE} to ${DEV_SIZE}"
	resize_online "${dev}" || \
	    log error "Unable to online-resize filesystem on ${dev}"
    else
	log info "Offline-resizing a ${fstype} filesystem on ${dev} from ${DEV_FSSIZE} to ${DEV_SIZE}"
	resize_offline "${dev}" || \
	    log error "Unable to offline-resize filesystem on ${dev}"
    fi
fi

# OK, time to mount or unmount
if [ "${PLUGIN_DOES_MOUNT:-off}" == "on" ]; then
    if [ "${action}" == "start" ]; then
	log info "Asking plugin to mount ${dev}"
	fs_mount "${dev}" || \
	    log fatal "Failed to mount ${dev}"
    elif [ "${action}" == "stop" ]; then
	log info "Asking plugin to unmount ${dev}"
	fs_umount "${dev}" || \
	    log fatal "Failed to unmount ${dev}"
    fi
elif [ -n "${AWSTAG_MOUNTPOINT}" ]; then
    if [ "${action}" == "start" ]; then
	# Is it already mounted?
	if [ -n "${DEV_MOUNTPOINT}" ]; then
	    log info "Device ${dev} already mounted on ${DEV_MOUNTPOINT}"
	    exit 0
	fi

	log info "Mounting ${dev} on ${AWSTAG_MOUNTPOINT}"

	# Get the mount options
	opts=$(options "${dev}")

	# Create the mountpoint if necessary
	if ! [ -d "${AWSTAG_MOUNTPOINT}" ]; then
	    # shellcheck disable=SC2174
	    mkdir -p -m "${AWSTAG_MODE:-755}" "${AWSTAG_MOUNTPOINT}" || \
		log fatal "Unable to create \"${AWSTAG_MOUNTPOINT}\" as mountpoint for ${dev}"
	    if [ -n "${AWSTAG_OWNER}" ]; then
		chown "${AWSTAG_OWNER}" "${AWSTAG_MOUNTPOINT}"
	    fi
	    if [ -n "${AWSTAG_GROUP}" ]; then
		chgrp "${AWSTAG_GROUP}" "${AWSTAG_MOUNTPOINT}"
	    fi
	fi

	# Mount it
	if [ -n "${opts}" ]; then
	    systemd-mount -o "${opts}" "${dev}" "${AWSTAG_MOUNTPOINT}" || \
		log fatal "Unable to mount ${dev} at ${AWSTAG_MOUNTPOINT}"
	else
	    systemd-mount "${dev}" "${AWSTAG_MOUNTPOINT}" || \
		log fatal "Unable to mount ${dev} at ${AWSTAG_MOUNTPOINT}"
	fi

	# Set up permissions on the mounted directory
	chmod "${AWSTAG_MODE:-755}" "${AWSTAG_MOUNTPOINT}"
	if [ -n "${AWSTAG_OWNER}" ]; then
	    chown "${AWSTAG_OWNER}" "${AWSTAG_MOUNTPOINT}"
	fi
	if [ -n "${AWSTAG_GROUP}" ]; then
	    chgrp "${AWSTAG_GROUP}" "${AWSTAG_MOUNTPOINT}"
	fi
    elif [ "${action}" == "stop" ]; then
	# Is it already unmounted?
	if [ -z "${DEV_MOUNTPOINT}" ]; then
	    log info "Device ${dev} already unmounted"
	    exit 0
	fi

	log info "Unmounting ${dev}"
	systemd-mount --umount "${dev}" || \
	    log fatal "Unable to unmount ${dev}"
    fi
fi
