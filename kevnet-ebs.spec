Name:		kevnet-ebs
Version:	%{ci_version}
Release:	%{ci_release}%{?dist}
Summary:	Automatically create and mount filesystems on EBS volumes

License:	Apache-2.0
URL:		https://gitlab.com/kev-net/rpms/kevnet-ebs/
Source0:	kevnet-ebs
Source1:	libexec
Source2:	kevnet-ebs@.service
Source3:	99-z-kevnet-ebs.rules

BuildArch:	noarch
BuildRequires:	systemd-rpm-macros, coreutils, sed
Requires:	systemd, systemd-udev, bash, util-linux, jq, awscli2, curl, coreutils, sed, xfsprogs, e2fsprogs, btrfs-progs

%description
The kevnet-ebs package contains a set of scripts and a systemd unit
file and udev rule file that together enable automatically creating,
resizing, and mounting filesystems based on tags associated with AWS
EBS volumes.  The tags specify such details as what filesystem type to
place on an EBS volume, where to mount it, and who should own it,
while size considerations are managed by comparing the filesystem size
to the size of the EBS block device.


%install
mkdir -p %{buildroot}/%{_sbindir}
sed -s 's|@LIBEXEC@|%{_libexecdir}/kevnet-ebs|g' < %{SOURCE0} > %{buildroot}/%{_sbindir}/kevnet-ebs
mkdir -p %{buildroot}/%{_libexecdir}
cp -a %{SOURCE1} %{buildroot}/%{_libexecdir}/kevnet-ebs
mkdir -p %{buildroot}/%{_unitdir}
sed -s 's|@PATH@|%{_sbindir}|g' < %{SOURCE2} > %{buildroot}/%{_unitdir}/kevnet-ebs@.service
mkdir -p %{buildroot}/%{_udevrulesdir}
cp %{SOURCE3} %{buildroot}/%{_udevrulesdir}/99-z-kevnet-ebs.rules


%files
%defattr(644,root,root)
%license LICENSE
%doc README.md
%attr(755,root,root) %{_sbindir}/kevnet-ebs
%dir %attr(755,root,root) %{_libexecdir}/kevnet-ebs
%{_libexecdir}/kevnet-ebs/*
%{_unitdir}/kevnet-ebs@.service
%{_udevrulesdir}/99-z-kevnet-ebs.rules


%post
# Basic reloads
systemctl daemon-reload
udevadm control --reload
udevadm trigger


%preun
# Stop kevnet-ebs for each xen block device
if [ "$1" -lt 1 ]; then
    devs=$(lsblk -o subsystems,path -J | jq -r '
               .blockdevices |
               map(select(.subsystems == "block:xen"))[] |
               .path
           ')
    for dev in ${devs}; do
        sysdev=/sys$(udevadm info --query=path "${dev}")
	sysd_unit=$(systemd-escape --path --template=kevnet-ebs@.service "${sysdev}")
	systemctl stop "${sysd_unit}"
    done
fi


%postun
# Basic reloads
systemctl daemon-reload
udevadm control --reload
udevadm trigger


%changelog
* Tue Sep 20 2022 Kevin L. Mitchell <klmitch@mit.edu>
- Initial creation of the RPM
