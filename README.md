# KevNet EBS

The KevNet EBS RPM contains the moving pieces necessary to perform actions on AWS EBS volumes from EC2 instances.  This consists of 3 basic parts:

1. A [`udev` rules file](99-z-kevnet-ebs.rules) that ensures that:
2. A [`systemd` service](kevnet-ebs@.service) is started, stopped, and reloaded, that ensures that:
3. A shell script is run with appropriate arguments to perform the necessary actions on the EBS volume.

The shell script uses the `lsblk` and `aws ec2 describe-volumes` commands to retrieve data about the current and desired state of the volume, and performs the requisite actions to:

1. Initialize a filesystem on the volume--only if necessary, existing filesystems are used as-is;
2. Resize the filesystem if necessary; and
3. Mount the filesystem in the appropriate place.

Note that, to utilize the `aws ec2 describe-volumes` command, the `awscli` RPM must be installed, and the instance must be configured with an instance profile that has the `ec2:DescribeVolumes` permissions.  (The RPM dependency will, of course, be a required dependency of the `kevnet-ebs` RPM.)

## `udev` Rules and `systemd` Service

The `udev` rules file is incredibly simple.  It merely ensures that devices with a subsystem of "block" nested under a "xen" subsystem are configured to want the `systemd` service `kevnet-ebs@.service`; the device path is automatically inserted after the `@`.

The `systemd` service is a little more complex.  It is configured as a "oneshot"-type service that has the `RemainAfterExit` flag turned on.  The start, reload, and stop scripts call the shell script with appropriate subcommands to communicate the action, followed by the path to the device.  (Note that this is not the `/dev/XXX` path, but rather the path in `/sys`.)  The unit is defined to bind with, and run after, the `systemd` device created by `udev`.  It is also configured to propagate reloads from the device, which ensures that the script will be called if the device is resized.  Finally, since the script needs to reach out to the AWS API, it is configured to run after the system's network is online.

## Script Architecture

The `kevnet-ebs` shell script is designed to support multiple filesystem types, and to perform both online and offline resizes.  The script makes use of filesystem-specific shell scripts in `libexec` that define functions for the basic operations: `init` initializes the filesystem; `resize_online` performs an on-line resize; `resize_offline` performs an off-line resize; and `options` returns options to pass to the mount command.  The `PLUGIN_DOES_MOUNT` variable, which defaults to `off`, can be set to `on` to instead call a `fs_mount` function to perform the mount (or mounts) needed, and `fs_umount` to perform unmounts.  (Note that if a plugin performs mounts directly, it should use the `systemd-mount` and `systemd-umount` commands for these.)

The script begins by converting the `/sys` path it was passed to the actual `/dev` path needed to actually act on the device.  (For the curious, this command is `udevadm info --query=property --property=DEVNAME --value ${syspath}`.)  After retrieving the device, it then runs `lsblk`, extracting all the values for the device to shell variables (e.g., the "FS Type" value becomes the value of the shell variable `DEV_FSTYPE`; note that these values *ARE NOT* exported, and so are not environment variables).  The `DEV_PKNAME` value is checked to ensure that the script is acting on the root device, and not a partition.

The script's next action is to use the `aws ec2 describe-volumes` command to select the volume data for the EBS volume attached to the `/dev` path.  It extracts just the tags associated with the volume itself (so, not the volume attachment) and saves them to shell variables with the `AWSTAG_` prefix, converting lower-case in the tag keys to uppercase.

If the `AWSTAG_FSTYPE` is set to the empty string, or if a plugin file with that value is not available in the `libexec` directory, then the script exits at this point, doing no further work.  (This is even if the block device _has_ a recognized filesystem.)

If the `DEV_FSTYPE` value is not set, then the plugin for the filesystem defined by `AWSTAG_FSTYPE` is loaded, and a filesystem is initialized.  If `DEV_FSTYPE` _is_ set, then the plugin defined by that value is loaded (`AWSTAG_FSTYPE` is ignored), and a check of the filesystem size to device size is performed, resulting in a resize if required.  Either way, the filesystem is then mounted, creating the mountpoint as desired.

## AWS Volume Tag Protocol

The following table describes the common tags.  Individual plugins may have additional tags that they recognize.

| Tag Key | Required | Plugin(s) | Purpose |
| --- | --- | --- | --- |
| Name | no | _none_ | The name of the volume.  This is not used by the script, but is frequently used to identify EBS volumes within AWS, and so is explicitly listed. |
| FSType | yes | _all_ | The filesystem type.  A plugin for the filesystem type must exist. |
| MountPoint | no | _all_ | The mountpoint.  The directory and its parents will be created if they do not exist. |
| Owner | no | _all_ | The user ID of the owner.  Defaults to "0". |
| Group | no | _all_ | The group ID of the group owner.  Defaults to "0". |
| Mode | no | _all_ | The octal representation of the mode for the mountpoint.  Defaults to "755". |

Note that any tags that are not recognized by the script are simply ignored, and no error will be generated.  Any plugin-specific tags will have the plugin name as a prefix for their keys, to avoid conflict with other tags the user may desire.  Finally, runs of disallowed characters in tag names--such as spaces--are converted into single underscores when converting a tag name into a shell variable.
